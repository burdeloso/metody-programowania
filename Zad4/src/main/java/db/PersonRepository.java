package db;

import java.util.List;

import domain.Person;

public interface PersonRepository extends Repository<Person> {

    public List<Person> withLastName(String lastName, PagingInfo page);
    public List<Person> withName(String name, PagingInfo page);
}
