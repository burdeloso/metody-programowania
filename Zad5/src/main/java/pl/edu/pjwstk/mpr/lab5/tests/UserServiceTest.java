package pl.edu.pjwstk.mpr.lab5.tests;

import junit.framework.Assert;
import junit.framework.TestCase;
import pl.edu.pjwstk.mpr.lab5.domain.*;
import pl.edu.pjwstk.mpr.lab5.service.UserService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsEmptyCollection.empty;


public class UserServiceTest extends TestCase {

    private User usr1;
    private User usr2;
    private Person per1;
    private Person per2;
    private Role role;
    private Permission perm;
    private Permission perm2;
    private List<Permission> permList;
    private List<Address> adrList;
    private List<User> usersToTest;

    public void initVars() {
        usr1 = new User();
        usr2 = new User();
        usr1.setName("Janek");
        usr2.setName("DlugggieImie");
        usr1.setPassword("gggg");
        per1 = new Person();
        per2 = new Person();
        role = new Role();
        perm = new Permission();
        perm.setName("perm1");
        perm2 = new Permission();
        perm2.setName("perm2");
        permList = new ArrayList<Permission>();
        permList.add(perm);
        permList.add(perm2);
        role.setPermissions(permList);
        per1.setRole(role);
        per2.setRole(role);
        per1.setName("Ajcek");
        per1.setSurname("Splacek");
        per2.setName("Aaluy");
        per2.setSurname("Salcuh");
        per2.setAge(40);
        usr2.setPersonDetails(per2);
        adrList = new ArrayList<Address>();
        adrList.add(new Address().setCity("Malbork").setCountry("Poland"));
        per1.setAge(15);
        per1.setAddresses(adrList);
        usr1.setPersonDetails(per1);
        usersToTest = new ArrayList<User>();
        usersToTest.add(usr1);
        usersToTest.add(usr2);

    }

    public void testFindUsersWhoHaveMoreThanOneAddress() throws Exception {
        usr1 = new User();
        usr1.setName("Janek");
        usr1.setPassword("gggg");
        per1 = new Person();
        adrList = new ArrayList<Address>();
        adrList.add(new Address().setCity("Malbork").setCountry("Poland"));
        adrList.add(new Address().setCity("Malbork").setCountry("Poland"));
        per1.setAddresses(adrList);
        usr1.setPersonDetails(per1);
        usersToTest = new ArrayList<User>();
        usersToTest.add(usr1);

        assertThat(UserService.findUsersWhoHaveMoreThanOneAddress(usersToTest), is(not(empty())));
    }

    public void testFindOldestPerson() throws Exception {
        initVars();
        Assert.assertEquals(UserService.findOldestPerson(usersToTest).getAge(), 40);
    }

    public void testFindUserWithLongestUsername() throws Exception {
        initVars();

        Assert.assertEquals(UserService.findUserWithLongestUsername(usersToTest).getName(), "DlugggieImie");

    }

    public void testGetNamesAndSurnamesCommaSeparatedOfAllUsersAbove18() throws Exception {
        initVars();
        Assert.assertEquals(UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(usersToTest), "Aaluy Salcuh");

    }

    public void testGetSortedPermissionsOfUsersWithNameStartingWithA() throws Exception {
        initVars();
        List<String> expected = Arrays.asList("perm1", "perm1", "perm2", "perm2");
        assertThat(UserService.getSortedPermissionsOfUsersWithNameStartingWithA(usersToTest), is(expected));
        // Assert.assertEquals(UserService.getSortedPermissionsOfUsersWithNameStartingWithA(usersToTest).toString(), "[perm1, perm1, perm2, perm2]");

    }

        
    public void testGroupUsersByRole() throws Exception {
        initVars();
        Assert.assertEquals(UserService.groupUsersByRole(usersToTest).get(role).get(0).getName(), "Janek");


    }

    public void testPartitionUserByUnderAndOver18() throws Exception {
        initVars();
        Assert.assertEquals(UserService.partitionUserByUnderAndOver18(usersToTest).get(true).get(0).getName(), "DlugggieImie");
    }
}