package service;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import domain.Address;

public class TestAddress {

    AddressManager addressManager = new AddressManager();

    private final static String STREET_1 = "Rolna";
    private final static String CITY_1 = "Giedajty";
    private final static String ZIPCODE_1 = "11-042";

    @Test
    public void checkConnection() {
        assertNotNull(addressManager.getConnection());
    }

    @Test
    public void checkAdding() {

        Address address = new Address(STREET_1, CITY_1, ZIPCODE_1);

        addressManager.clearAddresses();
        assertEquals(1, addressManager.addAddress(address));

        List<Address> addresses = addressManager.getAllAddresses();
        Address addressRetrieved = addresses.get(0);

        assertEquals(STREET_1, addressRetrieved.getStreet());
        assertEquals(ZIPCODE_1, addressRetrieved.getZipCode());
        assertEquals(CITY_1, addressRetrieved.getCity());

    }

}
