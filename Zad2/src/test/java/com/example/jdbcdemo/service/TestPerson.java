package service;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import domain.Person;

public class TestPerson {

    PersonManager personManager = new PersonManager();

    private final static String NAME_1 = "Stefan";
    private final static String LASTNAME_1 = "Kot";
    private final static String PHONENUMBER_1 = "12345678";


    @Test
    public void checkConnection(){
        assertNotNull(personManager.getConnection());
    }

    @Test
    public void checkAdding(){

        Person person = new Person(NAME_1, LASTNAME_1, PHONENUMBER_1);

        personManager.clearPersons();
        assertEquals(1,personManager.addPerson(person));

        List<Person> persons = personManager.getAllPersons();
        Person personRetrieved = persons.get(0);

        assertEquals(NAME_1, personRetrieved.getName());
        assertEquals(LASTNAME_1, personRetrieved.getLastName());
        assertEquals(PHONENUMBER_1, personRetrieved.getPhoneNumber());

    }

}
