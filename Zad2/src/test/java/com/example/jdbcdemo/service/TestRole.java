package service;


import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import domain.Role;

public class TestRole {


    RoleManager roleManager = new RoleManager();

    private final static String ROLE_1 = "uzytkownik";

    @Test
    public void checkConnection(){
        assertNotNull(roleManager.getConnection());
    }

    @Test
    public void checkAdding(){

        Role role = new Role(ROLE_1);

        roleManager.clearRoles();
        assertEquals(1,roleManager.addRole(role));

        List<Role> roles = roleManager.getAllRoles();
        Role roleRetrieved = roles.get(0);

        assertEquals(ROLE_1, roleRetrieved.getRole());

    }
}
