
public class Main {

	public static void main(String[] args) {
		
		Address adr = new Address("Olsztyn", "Rolna 2","11-042");
		Permission perm = new Permission("odczyt",true);
		Permission perm2 = new Permission("execute",false);
		Role role = new Role();
		role.addPermissions(perm);
		role.addPermissions(perm2);
		Person person = new Person("Adam","Burdalski","602283214");
		person.addAddress(adr);
		person.addRole(role);
		User usr = new User("on","ona");
		usr.setPerson(person);
		System.out.println(usr.toString());

	}

}
