import java.util.ArrayList;
import java.util.List;
public class Role {
	private List<Permission> permissions = new ArrayList<Permission>();
	
	public Role(){
		
	}

		public Role(List<Permission> permissions) {
			this.permissions = permissions;
			
		}

		public List<Permission> getPermissions(){
			return permissions;
		}

		public void setPermissions(List<Permission> permissions){
			this.permissions = permissions;
			
		}
		public void addPermissions(Permission perm){
			permissions.add(perm);
			
		}


}

