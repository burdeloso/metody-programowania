
public class Permission {

	private String name;
	private boolean perm;
	
	public Permission(String name, boolean perm){
		this.name = name;
		this.perm = perm;
	}
	public String getName(){
		return name;
		}
	public void setName(String name){
		this.name = name;
	}
	public boolean isPerm(){
		return perm;
	}
	public void setPerm(boolean perm){
		this.perm = perm;
	}
	

}
