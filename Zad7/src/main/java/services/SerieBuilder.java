package services;
import java.util.List;
import java.util.LinkedList;
import domain.*;

public class SerieBuilder {

	private ChartSerie chartSerie;

	public SerieBuilder() { 
		chartSerie = new ChartSerie(); 
	}

	public SerieBuilder addPoint(Point point) {
	List<Point> points = chartSerie.getPoints();
	if (points == null) {
		points = new LinkedList<Point>();
		chartSerie.setPoints(points);
		}
	points.add(point);
	return this;
	}

	public SerieBuilder addLabel(String label) {
	chartSerie.setLabel(label);
	return this;
	}

	public SerieBuilder addPoints(List<Point> points) {
	chartSerie.setPoints(points);
	return this;
	}

	public SerieBuilder setType(SerieType type) {
	chartSerie.setSerieType(type);
	return this;
	}

	public ChartSerie build() { 
		return chartSerie; 
	}
	
}
