package services;
import java.util.LinkedList;
import java.util.List;

import domain.ChartSerie;
import domain.ChartSettings;
import domain.ChartType;

public class ChartBuilder {
	
	private ChartSettings chartSettings;
	
	public ChartBuilder() { 
		chartSettings = new ChartSettings(); 
	}

	public ChartBuilder addSerie(ChartSerie chartSerie) {
		List<ChartSerie> series = chartSettings.getSeries();
		if (series == null) {
			series = new LinkedList<ChartSerie>();
			chartSettings.setSeries(series);
		}
		series.add(chartSerie);
		return this;
	}
	
	public ChartBuilder withSeries(List<ChartSerie> series)	{
		chartSettings.setSeries(series);
		return this;
	}
	
	public ChartBuilder withTitle(String title) {
		chartSettings.setTitle(title);
		return this;
	}
	
	public ChartBuilder withSubtitle(String subtitle) {
		chartSettings.setSubtitle(subtitle);
		return this;
	}
	
	public ChartBuilder withLegend() {
		chartSettings.setHasLegend(true);
		return this;
	}
	
	public ChartBuilder withType(ChartType type) {
		chartSettings.setChartType(type);
		return this;
	}
	
	public ChartSettings build() {
		return chartSettings;
	}
}
