import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;

import org.junit.*;
import domain.*;
import services.*;

public class SerieBuilderTest {

	private SerieBuilder serieBuilder;
	
	@Before
	public void before() {
			serieBuilder = new SerieBuilder();
	}
	
	@Test
	public void testIfPointIsAdded() {
		//GIVEN
		Point point = new Point(1,2);
		
		//WHEN
		ChartSerie serie = serieBuilder.addPoint(point).build();
		
		//THEN
		assertThat(serie.getPoints()).isNotNull().containsExactly(point);
	}
	
	@Test
	public void testIfLabelIsAdded() {
		//GIVEN
		String label = "l";
		
		//WHEN
		ChartSerie serie = serieBuilder.addLabel(label).build();
		
		//THEN
		assertThat(serie.getLabel()).isEqualTo(label);
	}
	
	@Test
	public void testIfPointsAreAdded() {
		//GIVEN
		Point point1 = new Point(1,2);
		Point point2 = new Point(2,3);
		Point point3 = new Point(3,4);
		
		List<Point> points = Arrays.asList(point1, point2, point3);
				
		//WHEN
		ChartSerie serie = serieBuilder.addPoints(points).build();
				
		//THEN
		assertThat(serie.getPoints()).isNotNull().containsAll(points);
	}
	
	@Test
	public void testIfTypeIsSet() {
		//GIVEN
		SerieType type = SerieType.Bar;
		
		//WHEN
		ChartSerie serie = serieBuilder.setType(type).build();
		
		//THEN
		assertThat(serie.getSerieType()).isEqualTo(type);
	}
	
}
