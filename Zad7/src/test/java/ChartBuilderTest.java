import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;

import org.junit.*;
import domain.*;
import services.*;

public class ChartBuilderTest {

	private ChartBuilder chartBuilder;
	
	@Before
	public void before() {
			chartBuilder = new ChartBuilder();
	}
	
	@Test
	public void testIfSeriesAreAdded() {
		//GIVEN
		String label1 = "l";
		String label2 = "x";
		Point point1 = new Point(1,2);
		Point point2 = new Point(2,2);
		SerieType serieType = SerieType.Line;
		List<Point> points = Arrays.asList(point1, point2);
		
		ChartSerie chartSerie1 = new ChartSerie();
		chartSerie1.setLabel(label1);
		chartSerie1.setPoints(points);
		chartSerie1.setSerieType(serieType);
		
		ChartSerie chartSerie2 = new ChartSerie();
		chartSerie2.setLabel(label2);
		chartSerie2.setPoints(points);
		chartSerie2.setSerieType(serieType);
		
		List<ChartSerie> chartSeries = Arrays.asList(chartSerie1, chartSerie2);
				
		//WHEN
		ChartSettings chart = chartBuilder.withSeries(chartSeries).build();
				
		//THEN
		assertThat(chart.getSeries()).isNotNull().isEqualTo(chartSeries);
	}
	
	@Test
	public void testIfTitleIsAdded() {
		//GIVEN
		String title = "t";
				
		//WHEN
		ChartSettings chart = chartBuilder.withTitle(title).build();
				
		//THEN
		assertThat(chart.getTitle()).isNotNull().isEqualTo(title);
	}
	
	@Test
	public void testIfSubtitleIsAdded() {
		//GIVEN
		String subtitle = "s";
				
		//WHEN
		ChartSettings chart = chartBuilder.withSubtitle(subtitle).build();
				
		//THEN
		assertThat(chart.getSubtitle()).isNotNull().isEqualTo(subtitle);
	}
	
	@Test
	public void testIfLegendIsSet() {
		//GIVEN
		boolean legend = true;
		
		//WHEN
		ChartSettings chart = chartBuilder.withLegend().build();
				
		//THEN
		assertThat(chart.isHasLegend()).isNotNull().isEqualTo(legend);
	}
	
	@Test
	public void testIfTypeIsSet() {
		//GIVEN
		ChartType type = ChartType.Point;
		
		//WHEN
		ChartSettings chart = chartBuilder.withType(type).build();
		
		//THEN
		assertThat(chart.getChartType()).isNotNull().isEqualTo(type);
	}
	
}
