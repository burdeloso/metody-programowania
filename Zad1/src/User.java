
public class User {
	private String login;
	private String password;
	private Person person;
	

	public User(String login, String password){
		this.login = login;
		this.password = password;
	}
	public String getLogin(){
		return login;
	}

	public void setLogin(String login){
		this.login = login;
	}

	public String getPassword(){
		return password;
	}
	
	public void setPassword(String password){
		this.password = password;		
	}
	public Person getPerson(){
		return person;
	}

	public void setPerson(Person person){
		this.person = person;
	}
	
	


}