package db;

import domain.Role;

import java.util.List;

public interface RoleRepository extends Repository<Role>	{
	public List<Role> withRole(String role, PagingInfo page);